from collections import deque

class Ticker:
    history_len = 10
    start_price = 0

    def __init__(self, name: str, price: int = None):
        self.name = name
        self.price = price if price else self.start_price
        self.history = deque()

    def add_delta_price(self, delta: int) -> int:
        self.price += delta
        self.history.append(self.price)
        if len(self.history) > self.history_len:
            self.history.popleft()
        return self.price

    def json(self) -> dict:
        return {'name': self.name,
                'price': self.price,
                'history': list(self.history)}

    def clear(self) -> None:
        self.price = self.start_price
        self.history.clear()


    def __setattr__(self, name, value):
        if name == 'price' and value < 0:
            value = 0
        object.__setattr__(self, name, value)


class TickersGenerator:

    def __call__(self, count: int):

        def get_name(count: int) -> str:
            return 'ticker_' + ('0' + str(count))[-2:]

        return {get_name(el): Ticker(get_name(el))
                for el in range(count)}


class Tickers:
    count = 100

    def __new__(cls, generator=TickersGenerator):
        if not hasattr(cls, 'instance'):
            cls.instance = object.__new__(cls)
            cls.instance.data = generator()(cls.count)
        return cls.instance

    def __getitem__(self, pos):
        return (self.data.get(pos) if self.data.get(pos)
                else list(self.data.values())[pos])

    def get_names(self) -> list:
        return list(self.data.keys())

    def clear(self) -> None:
        for el in self:
            el.clear()





