from random import random


class GenetarorMovement:

	def __call__(self):
		return -1 if random() < 0.5 else 1

