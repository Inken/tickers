#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import time
from flask import Flask
from flask_cors import CORS
from flask import request
from flask_sse import sse
from apscheduler.schedulers.background import BackgroundScheduler
from functools import partial
from tickers import Tickers, Ticker
from generator import GenetarorMovement


fhost = "0.0.0.0"
fport = 8001
app = Flask(__name__)
cors = CORS(app, supports_credentials=True, resources={r"*": {"origins": "*"}})

app.config["REDIS_URL"] = "redis://redis"
app.register_blueprint(sse, url_prefix="/stream")


def new_price_event(ticker: Ticker):
    with app.app_context():
        sse.publish(
            {
                "value": ticker.add_delta_price(GenetarorMovement()()),
                "time": time.time(),
            },
            type="price",
            channel=ticker.name,
        )


sched = BackgroundScheduler()

for el in Tickers():
    print('test')
    sched.add_job(partial(new_price_event, el), "interval", seconds=1)
sched.start()


@app.route("/tickers_names", methods=["POST"], endpoint="tickers_names")
def tickers_names():
    return json.dumps({"response": Tickers().get_names()}, default=str), 200


@app.route("/clear", methods=["POST"], endpoint="clear")
def clear_price():
    Tickers().clear()
    return json.dumps({"response": True}, default=str), 200


@app.route("/ticker_card", methods=["POST"], endpoint="ticker_card")
def ticker_card():
    req = request.get_data()
    ticker_name = json.loads(req if req else "{}").get("ticker_name")
    if not ticker_name:
        return json.dumps({"response": "Bad request"}, default=str), 400
    ticker = Tickers()[ticker_name]
    if not ticker:
        return json.dumps({"response": "Not found"}, default=str), 404
    return json.dumps({"response": ticker.json()}, default=str), 200ы


if __name__ == "__main__":
    print('удали меня')
    print('удали меня 2')
    app.run(host=fhost, port=fport)
