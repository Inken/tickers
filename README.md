# Tickers



## Описание

- Ивенты прилетают по SSE
- При выборе товара подгружается история
- Генератор обновляет цены для 100 товаров каждую секунду
- Ограничен переход в отрицательные значения цен
- Кнопка Рестарт цен" сбрасывает цены в api

## Демонстрация

#### [DEMO](https://tickers.service.oboi.ru) развернуто на kubernetes

#### Пример работы сборки в docker
![screen](https://251317.selcdn.ru/tmp/tickers.gif)

