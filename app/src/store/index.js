import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    backend_url: "http://localhost:8000/",
  },
  mutations: {},
  actions: {},
  modules: {},
});
